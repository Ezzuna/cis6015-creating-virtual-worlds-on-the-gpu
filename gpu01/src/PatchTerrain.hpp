#pragma once

#include "GLShaderFactory.hpp"


class TreeField;

// Forward declaration for cst::ArcballCamera
namespace cst {

	class ArcballCamera;
};

class PatchTerrain {

private:

	GLuint		vertexArrayObj;

	GLuint		extent;
	GLuint		numPoints;
	GLuint		numIndices;
	
	// Buffers for terrain model data
	GLuint		positionBuffer;
	GLuint		normalBuffer;
	GLuint		tangentBuffer;
	GLuint		bitangentBuffer;
	GLuint		textureCoordBuffer;
	GLuint		indexBuffer;

	// Uniform buffer for texture data
	GLuint		textureModelBuffer;

	// Shader
	GLProgramObject	surfaceShader;
	GLProgramObject	computeShader;

	GLuint					tempShader;

	// Textures
	GLuint		grassTexture;
	GLuint		waterTexture;
	GLuint		rockTexture;
	GLuint		snowTexture;
	GLuint		ProGenTexture;

	bool	passThrough;
	// On-terrain features
	TreeField*	treeField;

	void loadShader();

public:

	PatchTerrain(const GLuint extent, const glm::vec2& domainCoord, const glm::vec2& domainExtent, int numOctaves, GLuint procTexture);

	PatchTerrain(GLuint initTexture);

	void render(cst::ArcballCamera* camera, GLuint procTexture);
};

