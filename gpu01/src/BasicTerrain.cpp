#include <pch.h>
#include "BasicTerrain.hpp"
#include "Noise/PerlinNoise.hpp"
#include "ArcballCamera.hpp"

using namespace std;
using namespace cst;


// ** Model texture data needed for terrain
/*
	From OpenGL Superbible 7th edition: Any type consuming N bytes in a buffer begins on an N-byte boundary within that buffer. That means that standard GLSL types such as int, float, and bool (which are all defined to be 32-bit or 4-byte quantities) begin on multiples of 4 bytes. A vector of these types of length 2 always begins on a 2N-byte boundary. For example, a vec2, which is 8 bytes long in memory, always starts on an 8-byte boundary. Three- and four-element vectors always start on a 4N-byte boundary; vec3 and vec4 types start on 16-byte boundaries, for instance. Each member of an array of scalar or vector types (arrays of int or vec3, for example) always starts on a boundary defined by these same rules, but rounded up to the alignment of a vec4. In particular, this means that arrays of anything but vec4 (and N � 4 matrices) won�t be tightly packed, but instead will have a gap between each of the elements. Matrices are essentially treated like short arrays of vectors, and arrays of matrices are treated like very long arrays of vectors. Finally, structures and arrays of structures have additional packing requirements; the whole structure starts on the boundary required by its largest member, rounded up to the size of a vec4.
	*/

// C++ will pack array elements in cases such as: alignas(16) GLint myarray[] = { ... };
// So need to align each element - to do this setup wrapper structure with appropriate alignment.
// Not ideal as access becomes: auto x = myArray[i].value;
// However, helps simplify alignment structure of memory footprint
struct alignas(16) int_element16 {
	GLint value;
};

struct alignas(16) Texture {

	alignas(8) GLuint64 texture;
	alignas(8) glm::vec2 scale;
};

struct alignas(16) TerrainTexture {

	Texture grass, rock, snow;
	/*alignas(8) GLuint64		grassTextureHandle; // ... GLuint64 -> 8 (except after array which has vec4 align and size characteristics)
	alignas(8) GLuint64		rockTextureHandle;
	alignas(8) GLuint64		snowTextureHandle;

	alignas(8) glm::vec2	grassScale; // vec2 types just 2*sizeof GLSL type size: glm::vec2 -> 8 (2 floats)
	alignas(8) glm::vec2	rockScale;
	alignas(8) glm::vec2	snowScale;
	*/
	// vec3 different - also 4N byte aligned like vec4
	
};

BasicTerrain::BasicTerrain(const GLuint extent) {

	PerlinNoise* noiseGen = new PerlinNoise();

	this->extent = extent;
	numPoints = extent * extent;

	// Create position array
	GLuint positionArraySize = (extent * extent) * sizeof(glm::vec4);
	glm::vec4* positionArray = (glm::vec4*)malloc(positionArraySize);

	glm::vec3* normalArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec3* tangentArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec3* bitangentArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec2* textureCoordArray = (glm::vec2*)malloc(numPoints * sizeof(glm::vec2)); // **

	// Create index array
	numIndices = (extent - 1) * (extent - 1) * 2 * 3;
	GLuint indexArraySize = numIndices * sizeof(GLuint);
	GLuint* indexArray = (GLuint*)malloc(indexArraySize);

	// Populate arrays
	
	// Evaluate Perlin noise over a given domain...
	float domainStart = 0.0f;
	float domainEnd = 3.0f;
	float domainSize = domainEnd - domainStart;
	float domainStep = domainSize / float(extent);

	// ... and frequency range
	int numOctaves = 4;
	
	float v = domainStart;
	for (GLuint i = 0, z = 0; z < extent; z++, v += domainStep) {

		float u = domainStart;
		for (GLuint x = 0; x < extent; x++, i++, u += domainStep) {

			float noiseValue = 0.0f;
			float uLocal = u;
			float vLocal = v;
			float freq = 1.0f;
			glm::vec2 dnTotal = glm::vec2(0.0f, 0.0f);
			glm::vec2 dn = glm::vec2(0.0f, 0.0f);
			for (int f = 0; f < numOctaves; f++, uLocal*=2.0f, vLocal*=2.0f, freq*=2.0f) {
				
				// Noise with fractal power spectrum (frequency distribution)
				noiseValue += noiseGen->noise(uLocal, vLocal, 1.0f, &dn) * (1.0f / freq);
			
				dnTotal += dn * (1.0f / freq);
			}

			positionArray[i].x = u;
			positionArray[i].y = noiseValue;
			positionArray[i].z = v;
			positionArray[i].w = 1.0f;

			tangentArray[i] = glm::normalize(glm::vec3(1.0f, dnTotal.x, 0.0f));
			bitangentArray[i] = glm::normalize(glm::vec3(0.0f, dnTotal.y, 1.0f));
			normalArray[i] = glm::normalize(glm::cross(bitangentArray[i], tangentArray[i]));
			textureCoordArray[i] = glm::vec2(float(x) / float(extent - 1), float(z) / float(extent - 1)); //**
		}
	}
	
	// Calculate indicies for each triangular face
	GLuint* iPtr = indexArray;
	for (GLuint z = 0; z < (extent - 1); z++) {

		for (GLuint x = 0; x < (extent - 1); x++, iPtr += 6) {

			iPtr[0] = z * extent + x;
			iPtr[1] = (z + 1) * extent + x;
			iPtr[2] = (z + 1) * extent + x + 1;

			iPtr[3] = iPtr[0];
			iPtr[4] = iPtr[2];
			iPtr[5] = z * extent + x + 1;
		}
	}
	

	// GPU structures

	// Create VAO to encapsulate bindings
	glCreateVertexArrays(1, &vertexArrayObj);

	// Setup and "wire up" position buffer to packet slot 0
	glCreateBuffers(1, &positionBuffer);
	glNamedBufferStorage(positionBuffer, positionArraySize, positionArray, 0);

	glVertexArrayVertexBuffer(vertexArrayObj, 0, positionBuffer, 0, sizeof(glm::vec4));
	glVertexArrayAttribBinding(vertexArrayObj, 0, 0);
	glVertexArrayAttribFormat(vertexArrayObj, 0, 4, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 0);

	glCreateBuffers(1, &normalBuffer);
	glNamedBufferStorage(normalBuffer, extent * extent * sizeof(glm::vec3), normalArray, 0);
	glVertexArrayVertexBuffer(vertexArrayObj, 1, normalBuffer, 0, sizeof(glm::vec3));
	glVertexArrayAttribBinding(vertexArrayObj, 1, 1);
	glVertexArrayAttribFormat(vertexArrayObj, 1, 3, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 1);

	// ** Setup texture coord buffer
	glCreateBuffers(1, &textureCoordBuffer);
	glNamedBufferStorage(textureCoordBuffer, extent * extent * sizeof(glm::vec2), textureCoordArray, 0);
	glVertexArrayVertexBuffer(vertexArrayObj, 2, textureCoordBuffer, 0, sizeof(glm::vec2));
	glVertexArrayAttribBinding(vertexArrayObj, 2, 2);
	glVertexArrayAttribFormat(vertexArrayObj, 2, 2, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 2);

	// Setup index buffer
	glCreateBuffers(1, &indexBuffer);
	glNamedBufferStorage(indexBuffer, indexArraySize, indexArray, 0);
	glVertexArrayElementBuffer(vertexArrayObj, indexBuffer);

	// Clean-up
	free(positionArray);
	free(indexArray);

	
	surfaceShader = GLShaderFactory::CreateProgramObject(
		GLShaderFactory::CreateShader(GL_VERTEX_SHADER, string("shaders\\basic_terrain.vs.txt")),
		GLShaderFactory::CreateShader(GL_FRAGMENT_SHADER, string("shaders\\basic_terrain.fs.txt"))
	);

	wireframeShader = GLShaderFactory::CreateProgramObject(
		GLShaderFactory::CreateShader(GL_VERTEX_SHADER, string("shaders\\terrain_wireframe.vs.txt")),
		GLShaderFactory::CreateShader(GL_FRAGMENT_SHADER, string("shaders\\terrain_wireframe.fs.txt")),
		GLShaderFactory::CreateShader(GL_GEOMETRY_SHADER, string("shaders\\terrain_wireframe.gs.txt"))
	);

	/*wireframeShader = setupShaders(
		string("shaders\\terrain_wireframe.vs.txt"),
		string("shaders\\terrain_wireframe.gs.txt"),
		string("shaders\\terrain_wireframe.fs.txt")
	);*/

	// ** Load terrain textures to apply multi-texturing
	grassTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\Hodges_G_Grass3.jpg", TextureProperties(true));
	rockTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\Hodges_G_MountainRock1.jpg", TextureProperties(true));
	snowTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\LLoyd_Jordan_snow1.jpg", TextureProperties(true));


	// ** Populate texture structure
	
	/*TerrainTexture* t1 = (TerrainTexture*)calloc(1, sizeof(TerrainTexture));
	GLuint* tptr = (GLuint*)&(t1->f1);
	*tptr = 0xFFFFFFFF;
	unsigned long long *hptr = (unsigned long long*)&(t1->grassTextureHandle);
	size_t foo = sizeof(unsigned long long);
	*hptr = 0xA0A1A2A3A4A5A6A7;
	cout << "max align = " << alignof(max_align_t) << '\n'; // ?? 8 ??
	unsigned long long a1 = (unsigned long long)&(t1->v3foo);
	unsigned long long a2 = (unsigned long long)&(t1->f2);
	unsigned long long a3 = (unsigned long long)&(t1->myArray[0]);
	
	cout << "v3foo at " << a1 << endl;
	cout << "f2 at " << a2 << " with offset from v3foo start of " << (a2 - a1) << endl;
	cout << "myArray[0] at " << a3 << " with offset from f2 start of " << (a3 - a2) << endl;
	*/

	TerrainTexture T;
	//texture.grassTextureHandle = glGetTextureHandleARB(grassTexture);
	//texture.rockTextureHandle = glGetTextureHandleARB(rockTexture);
	//texture.snowTextureHandle = glGetTextureHandleARB(snowTexture);
	//texture.grassScale = glm::vec2(6.0f, 6.0f);
	//texture.rockScale = glm::vec2(6.0f, 6.0f);
	//texture.snowScale = glm::vec2(6.0f, 6.0f);

	T.grass.texture = glGetTextureHandleARB(grassTexture);
	T.rock.texture = glGetTextureHandleARB(rockTexture);
	T.snow.texture = glGetTextureHandleARB(snowTexture);
	T.grass.scale = glm::vec2(6.0f, 6.0f);
	T.rock.scale = glm::vec2(6.0f, 6.0f);
	T.snow.scale = glm::vec2(6.0f, 6.0f);

	// ** Setup texture scale buffer
	glCreateBuffers(1, &textureModelBuffer);
	glNamedBufferStorage(textureModelBuffer, sizeof(TerrainTexture), &T, 0);

	//glMakeTextureHandleResidentARB(texture.grassTextureHandle);
	//glMakeTextureHandleResidentARB(texture.rockTextureHandle);
	//glMakeTextureHandleResidentARB(texture.snowTextureHandle);
	glMakeTextureHandleResidentARB(T.grass.texture);
	glMakeTextureHandleResidentARB(T.rock.texture);
	glMakeTextureHandleResidentARB(T.snow.texture);

	// ** test - check uniform block limits
	GLint maxFragUniformBlocks, maxUniformBlockSize, maxBindings;
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &maxFragUniformBlocks);
	glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &maxUniformBlockSize);
	glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &maxBindings);

	cout << "Max frag shader uniform blocks = " << maxFragUniformBlocks << endl; // on nVidia GTX1070 - 14
	cout << "Max uniform block size = " << maxUniformBlockSize << endl;// on nVidia GTX1070 - 64k
	cout << "Max uniform bindings = " << maxBindings << endl;

	GLint vs_fsTU, vsTU, gsTU, fsTU, csTU;
	
	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &vsTU); // Total texture units accessible from VS
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &fsTU); // Total texture units accessible from FS
	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &vs_fsTU); // Total texture units for VS + FS combined (same unit access in VS and FS counts as 2)
	glGetIntegerv(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS, &csTU);
	glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &gsTU);

	cout << "TU in VS = " << vsTU << endl;
	cout << "TU in GS = " << gsTU << endl;
	cout << "TU in FS = " << fsTU << endl;
	cout << "TU in CS = " << csTU << endl;
	cout << "TU Combined in VS+FS = " << vs_fsTU << endl;

	//
	//
}


void BasicTerrain::render(ArcballCamera* camera) {

	// ** Bind uniform buffer with texture handles and scale factors
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, textureModelBuffer);

	// Get the camera properties we need
	glm::mat4 viewTransform = camera->viewTransform();
	glm::mat4 projectionTransform = camera->projectionTransform();
	float nearPlaneDist = camera->getViewFrustum().nearPlaneDistance();
	float farPlaneDist = camera->getViewFrustum().farPlaneDistance();

	glBindVertexArray(vertexArrayObj);

	// Bind surface shader and assign uniform values
	GLuint shader = surfaceShader->useProgramObject();// surfaceShader->useProgram();

	static GLint viewLocation = glGetUniformLocation(shader, "viewMatrix");
	static GLint projLocation = glGetUniformLocation(shader, "projMatrix");
	static GLint nearLocation = glGetUniformLocation(shader, "near");
	static GLint farLocation = glGetUniformLocation(shader, "far");
	
	glUniformMatrix4fv(viewLocation, 1, GL_FALSE, (const GLfloat*)&viewTransform);
	glUniformMatrix4fv(projLocation, 1, GL_FALSE, (const GLfloat*)&projectionTransform);
	glUniform1f(nearLocation, nearPlaneDist);
	glUniform1f(farLocation, farPlaneDist);

	// Draw terrain
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (const GLvoid*)0);

	//
	// ** Exercise - update the wireframe shaders to linearise depth as well!!!
	//

	glm::mat4 viewProjMatrix = projectionTransform * viewTransform;

	shader = wireframeShader->useProgramObject();
	auto mvpLocation = glGetUniformLocation(shader, "mvpMatrix");

	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&viewProjMatrix);

	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (const GLvoid*)0);
}