#pragma once

#include "GLShaderFactory.hpp"

// Forward declaration for cst::ArcballCamera
namespace cst {

	class ArcballCamera;
};

class BasicTerrain {

private:

	GLuint		vertexArrayObj;

	GLuint		extent;
	GLuint		numPoints;
	GLuint		numIndices;

	GLuint		positionBuffer;
	GLuint		normalBuffer;
	GLuint		tangentBuffer;
	GLuint		bitangentBuffer;
	GLuint		textureCoordBuffer; // **
	GLuint		textureModelBuffer; // **

	GLuint		indexBuffer;

	GLProgramObject	surfaceShader;

	GLProgramObject wireframeShader;

	// ** Textures
	GLuint		grassTexture;
	GLuint		rockTexture;
	GLuint		snowTexture;

public:

	BasicTerrain(const GLuint extent);

	void render(cst::ArcballCamera* camera);
};