//
// Load and compile OpenGL Shading Language (GLSL) shaders
//

#pragma once

#if 0
template <GLenum shaderType>
class GLShader {

	GLuint		shader;

	// Private API

	const string* loadShaderSourceStringFromFile(const string& filePath) {

		string* sourceString = nullptr;

		const char* file_str = filePath.c_str();

		struct stat fileStatus;
		int file_error = stat(file_str, &fileStatus);

		if (file_error == 0) {

			_off_t fileSize = fileStatus.st_size;

			char* src = (char*)calloc(fileSize + 1, 1); // add null-terminator character at end of string

			if (src) {

				ifstream shaderFile(file_str);

				if (shaderFile.is_open()) {

					shaderFile.read(src, fileSize);
					sourceString = new string(src);

					shaderFile.close();
				}

				// dispose of local resources
				free(src);
			}
		}

		// return pointer to new source string
		return sourceString;
	}

	void printSourceListing(const string& sourceString, bool showLineNumbers) {

		const char* srcPtr = sourceString.c_str();
		const char* srcEnd = srcPtr + sourceString.length();

		size_t lineIndex = 0;

		while (srcPtr < srcEnd) {

			if (showLineNumbers) {

				cout.fill(' ');
				cout.width(4);
				cout << dec << ++lineIndex << " > ";
			}

			size_t substrLength = strcspn(srcPtr, "\n");

			cout << string(srcPtr, 0, substrLength) << endl;

			srcPtr += substrLength + 1;
		}
	}

public:

	GLShader(const char* filename) {

		shader = 0;
		const string* sourceString = loadShaderSourceStringFromFile(shaderFilePath);

		if (!sourceString) {

			cout << loader_info[shaderType].capName << " shader " << shaderFilePath << " source not found." << endl;
			return loader_info[shaderType].errSourceNotFound;
		}

		const char* src = sourceString->c_str();

		if (!src) {

			cout << loader_info[shaderType].capName << " shader " << shaderFilePath << " source not found." << endl;

			delete sourceString;
			return loader_info[shaderType].errSourceNotFound;
		}

		GLuint shader = glCreateShader(shaderType);

		if (shader == 0) {

			delete sourceString;
			return loader_info[shaderType].errShaderObjectCreation;;
		}

		glShaderSource(shader, 1, static_cast<const GLchar**>(&src), 0);
		glCompileShader(shader);

		GLint compileStatus;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

		if (compileStatus == 0) {

			// Compilation error occurred

			cout << "The " << loader_info[shaderType].name << " shader " << shaderFilePath << " could not be compiled successfully..." << endl;
			cout << loader_info[shaderType].capName << " shader source code...\n\n";

			printSourceListing(*(sourceString));

			// report compilation error log

			cout << "\n<" << loader_info[shaderType].name << " shader compiler errors--------------------->\n\n";
			reportShaderInfoLog(shader);
			cout << "<-----------------end " << loader_info[shaderType].name << " shader compiler errors>\n\n\n";

			delete sourceString;
			glDeleteShader(shader);

			return loader_info[shaderType].errShaderCompilation;
		}

		*shaderObject = shader;

		return ShaderError::GLSL_OK;

	}
};

/*

Factory.CreateProgramObject( GLShaderStage<GL_VERTEX_SHADER>("filename"),

*/


// Factory class for creating shader program objects
class ShaderProgramFactory {


};
#endif

// Declare GLSL setup return / error codes
enum class ShaderError : uint8_t {

	GLSL_OK = 0,

	GLSL_SHADER_SOURCE_NOT_FOUND,
	GLSL_VERTEX_SHADER_SOURCE_NOT_FOUND,
	GLSL_TESS_CONTROL_SHADER_SOURCE_NOT_FOUND,
	GLSL_TESS_EVALUATION_SHADER_SOURCE_NOT_FOUND,
	GLSL_GEOMETRY_SHADER_SOURCE_NOT_FOUND,
	GLSL_FRAGMENT_SHADER_SOURCE_NOT_FOUND,
	GLSL_COMPUTE_SHADER_SOURCE_NOT_FOUND,

	GLSL_SHADER_OBJECT_CREATION_ERROR,
	GLSL_VERTEX_SHADER_OBJECT_CREATION_ERROR,
	GLSL_TESS_CONTROL_SHADER_OBJECT_CREATION_ERROR,
	GLSL_TESS_EVALUATION_SHADER_OBJECT_CREATION_ERROR,
	GLSL_GEOMETRY_SHADER_OBJECT_CREATION_ERROR,
	GLSL_FRAGMENT_SHADER_OBJECT_CREATION_ERROR,
	GLSL_COMPUTE_SHADER_OBJECT_CREATION_ERROR,

	GLSL_SHADER_COMPILE_ERROR,
	GLSL_VERTEX_SHADER_COMPILE_ERROR,
	GLSL_TESS_CONTROL_SHADER_COMPILE_ERROR,
	GLSL_TESS_EVALUATION_SHADER_COMPILE_ERROR,
	GLSL_GEOMETRY_SHADER_COMPILE_ERROR,
	GLSL_FRAGMENT_SHADER_COMPILE_ERROR,
	GLSL_COMPUTE_SHADER_COMPILE_ERROR,

	GLSL_PROGRAM_OBJECT_CREATION_ERROR,
	GLSL_PROGRAM_OBJECT_LINK_ERROR

};


// Basic shader object creation function takes a path to a vertex shader file and fragment shader file and returns a bound and linked shader program object
GLuint setupShaders(const std::string& vsPath,
	const std::string& gsPath,
	const std::string& fsPath,
	ShaderError* error_result = NULL);

//GLuint setupComputeShader(const std::string& csPath, ShaderError* error_result = NULL);


