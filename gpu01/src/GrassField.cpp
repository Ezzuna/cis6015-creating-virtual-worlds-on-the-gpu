#include <pch.h>
#include "GrassField.hpp"

using namespace std;

GrassField::GrassField(const GLuint numPoints) {

	this->numPoints = numPoints;

	GLuint positionArraySize = numPoints * sizeof(glm::vec4);
	GLuint colourArraySize = numPoints * sizeof(glm::vec4);
	GLuint heightArraySize = numPoints * sizeof(float);
	GLuint indexArraySize = numPoints * sizeof(GLuint);

	glm::vec4* positionArray = (glm::vec4*)malloc(positionArraySize);
	glm::vec4* colourArray = (glm::vec4*)malloc(colourArraySize);
	float* heightArray = (float*)malloc(heightArraySize);
	GLuint* indexArray = (GLuint*)malloc(indexArraySize);

	random_device rd;
	mt19937 mt = mt19937(rd());
	uniform_real_distribution<float> positionDist = uniform_real_distribution<float>(-2.0f, 2.0f);

	// populate arrays
	for (GLuint i = 0; i < numPoints; i++) {

		positionArray[i].x = positionDist(mt);
		positionArray[i].y = 0.0f;
		positionArray[i].z = positionDist(mt);
		positionArray[i].w = 1.0f;

		colourArray[i].r = 0.0f;
		colourArray[i].g = 1.0f;
		colourArray[i].b = 0.0f;
		colourArray[i].a = 0.6f;

		heightArray[i] = 2.0f;

		indexArray[i] = i;
	}

	// GPU structures

	glCreateVertexArrays(1, &vertexArrayObj);

	// Setup and "wire up" position buffer to packet slot 0
	glCreateBuffers(1, &positionBuffer);
	glNamedBufferStorage(positionBuffer, positionArraySize, positionArray, 0);

	glVertexArrayVertexBuffer(vertexArrayObj, 0, positionBuffer, 0, sizeof(glm::vec4));
	glVertexArrayAttribBinding(vertexArrayObj, 0, 0);
	glVertexArrayAttribFormat(vertexArrayObj, 0, 4, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 0);


	// Setup and "wire up" colour buffer to packet slot 0
	glCreateBuffers(1, &colourBuffer);
	glNamedBufferStorage(colourBuffer, colourArraySize, colourArray, 0);

	glVertexArrayVertexBuffer(vertexArrayObj, 1, colourBuffer, 0, sizeof(glm::vec4));
	glVertexArrayAttribBinding(vertexArrayObj, 1, 1);
	glVertexArrayAttribFormat(vertexArrayObj, 1, 4, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 1);


	// Setup and "wire up" height buffer to packet slot 0
	glCreateBuffers(1, &heightBuffer);
	glNamedBufferStorage(heightBuffer, heightArraySize, heightArray, 0);

	glVertexArrayVertexBuffer(vertexArrayObj, 5, heightBuffer, 0, sizeof(float));
	glVertexArrayAttribBinding(vertexArrayObj, 5, 5);
	glVertexArrayAttribFormat(vertexArrayObj, 5, 1, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 5);


	// Setup index buffer
	glCreateBuffers(1, &indexBuffer);
	glNamedBufferStorage(indexBuffer, indexArraySize, indexArray, 0);
	glVertexArrayElementBuffer(vertexArrayObj, indexBuffer);


	// Clean-up
	free(positionArray);
	free(colourArray);
	free(heightArray);
	free(indexArray);


	// Setup shader
	shader = setupShaders(
		string("shaders\\grass.vs.txt"),
		string("shaders\\grass.gs.txt"),
		string("shaders\\grass.fs.txt")
	);

}


void GrassField::render(const glm::mat4& viewProjMatrix) {

	glUseProgram(shader);

	static GLint mvpLocation = glGetUniformLocation(shader, "mvpMatrix");
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&viewProjMatrix);

	glBindVertexArray(vertexArrayObj);
	glDrawElements(GL_POINTS, numPoints, GL_UNSIGNED_INT, (const GLvoid*)0);
}