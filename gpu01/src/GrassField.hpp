#pragma once

class GrassField {

private:

	GLuint		vertexArrayObj;

	GLuint		numPoints;
	GLuint		positionBuffer;
	GLuint		colourBuffer;
	GLuint		heightBuffer;
	GLuint		indexBuffer;

	GLuint		texture;
	GLuint		shader;

public:

	GrassField(const GLuint numPoints);

	void render(const glm::mat4& viewProjMatrix);
};