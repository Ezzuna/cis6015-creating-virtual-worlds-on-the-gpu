#include <pch.h>
#include "PatchTerrain.hpp"
#include "Noise/PerlinNoise.hpp"
#include "ArcballCamera.hpp"
#include "TreeField.hpp"

using namespace std;
using namespace cst;


// ** Model texture data needed for terrain
/*
	From OpenGL Superbible 7th edition: Any type consuming N bytes in a buffer begins on an N-byte boundary within that buffer. That means that standard GLSL types such as int, float, and bool (which are all defined to be 32-bit or 4-byte quantities) begin on multiples of 4 bytes. A vector of these types of length 2 always begins on a 2N-byte boundary. For example, a vec2, which is 8 bytes long in memory, always starts on an 8-byte boundary. Three- and four-element vectors always start on a 4N-byte boundary; vec3 and vec4 types start on 16-byte boundaries, for instance. Each member of an array of scalar or vector types (arrays of int or vec3, for example) always starts on a boundary defined by these same rules, but rounded up to the alignment of a vec4. In particular, this means that arrays of anything but vec4 (and N � 4 matrices) won�t be tightly packed, but instead will have a gap between each of the elements. Matrices are essentially treated like short arrays of vectors, and arrays of matrices are treated like very long arrays of vectors. Finally, structures and arrays of structures have additional packing requirements; the whole structure starts on the boundary required by its largest member, rounded up to the size of a vec4.
	*/

	// C++ will pack array elements in cases such as: alignas(16) GLint myarray[] = { ... };
	// So need to align each element - to do this setup wrapper structure with appropriate alignment.
	// Not ideal as access becomes: auto x = myArray[i].value;
	// However, helps simplify alignment structure of memory footprint
struct alignas(16) int_element16 {
	GLint value;
};

struct alignas(16) Texture {

	alignas(8) GLuint64 texture;
	alignas(8) glm::vec2 scale;
};

struct alignas(16) TerrainTexture {

	Texture grass, water, rock, snow, proGen ;
};

// Generate tessellated terrain patch
// Parameters:
// extent - number of vertices on terrain patch
// domainCoord - base domain coordinate to evaluate from (not base vertex coord in world coords!)
// domainExtent - Range of domain to evaluate of 'extent' vertices

PatchTerrain::PatchTerrain(const GLuint extent, const glm::vec2& domainCoord, const glm::vec2& domainExtent, int numOctaves, GLuint procTexture) {

	ProGenTexture = procTexture;

	PerlinNoise* noiseGen = new PerlinNoise();

	this->extent = extent;

	numPoints = extent * extent;

	// Create arrays to store per-vertex data
	glm::vec4* positionArray = (glm::vec4*)malloc(numPoints * sizeof(glm::vec4));
	glm::vec3* normalArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec3* tangentArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec3* bitangentArray = (glm::vec3*)malloc(numPoints * sizeof(glm::vec3));
	glm::vec2* textureCoordArray = (glm::vec2*)malloc(numPoints * sizeof(glm::vec2));

	// Create index array
	numIndices = (extent - 1) * (extent - 1) * 2 * 3;
	GLuint indexArraySize = numIndices * sizeof(GLuint);
	GLuint* indexArray = (GLuint*)malloc(indexArraySize);

	// Populate arrays

	// Calculate domain step for each vertex...
	glm::vec2 domainStep = glm::vec2(domainExtent.x / float(extent - 1), domainExtent.y / float(extent - 1));

	glm::vec2 dnTotal = glm::vec2(0.0f, 0.0f);

	// Setup lambda (closure) to calculate fractal noise.
	auto fractalLambda = [&](float u, float v)->float {

		float noiseValue = 0.0f;
		glm::vec2 dn = glm::vec2(0.0f, 0.0f);
		float freq = 1.0f;
		
		for (int f = 0; f < numOctaves; f++, u *= 2.0f, v *= 2.0f, freq *= 2.0f) {

			// Noise with fractal power spectrum (frequency distribution)
			noiseValue += noiseGen->noise(u, v, 1.0f, &dn) * (1.0f / freq);
			dnTotal += dn * (1.0f / freq);
		}

		return noiseValue;
	};

	float v = domainCoord.y;
	for (GLuint i = 0, z = 0; z < extent; z++, v += domainStep.y) {

		float u = domainCoord.x;
		for (GLuint x = 0; x < extent; x++, i++, u += domainStep.x) {
			
			dnTotal = glm::vec2(0.0f, 0.0f);
			float noiseValue = fractalLambda(u, v);
			//std::cout << noiseValue;
			positionArray[i].x = u;
			if(noiseValue <= -0.21f)
				positionArray[i].y = -0.21f; //Create a floor for our water
			else
				positionArray[i].y = noiseValue;
			positionArray[i].z = v;
			positionArray[i].w = 1.0f;

			tangentArray[i] = glm::normalize(glm::vec3(1.0f, dnTotal.x, 0.0f));
			bitangentArray[i] = glm::normalize(glm::vec3(0.0f, dnTotal.y, 1.0f));
			normalArray[i] = glm::normalize(glm::cross(bitangentArray[i], tangentArray[i]));
			textureCoordArray[i] = glm::vec2(float(x) / float(extent - 1), float(z) / float(extent - 1));
		}
	}

	// Calculate indicies for each triangular face
	GLuint* iPtr = indexArray;
	for (GLuint z = 0; z < (extent - 1); z++) {

		for (GLuint x = 0; x < (extent - 1); x++, iPtr += 6) {

			iPtr[0] = z * extent + x;
			iPtr[1] = (z + 1) * extent + x;
			iPtr[2] = (z + 1) * extent + x + 1;

			iPtr[3] = iPtr[0];
			iPtr[4] = iPtr[2];
			iPtr[5] = z * extent + x + 1;
		}
	}


	// GPU structures

	// Create VAO to encapsulate bindings
	glCreateVertexArrays(1, &vertexArrayObj);

	// Setup and "wire up" position buffer to packet slot 0
	glCreateBuffers(1, &positionBuffer);
	glNamedBufferStorage(positionBuffer, numPoints * sizeof(glm::vec4), positionArray, 0);

	glVertexArrayVertexBuffer(vertexArrayObj, 0, positionBuffer, 0, sizeof(glm::vec4));
	glVertexArrayAttribBinding(vertexArrayObj, 0, 0);
	glVertexArrayAttribFormat(vertexArrayObj, 0, 4, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 0);

	glCreateBuffers(1, &normalBuffer);
	glNamedBufferStorage(normalBuffer, numPoints * sizeof(glm::vec3), normalArray, 0);
	glVertexArrayVertexBuffer(vertexArrayObj, 1, normalBuffer, 0, sizeof(glm::vec3));
	glVertexArrayAttribBinding(vertexArrayObj, 1, 1);
	glVertexArrayAttribFormat(vertexArrayObj, 1, 3, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 1);

	// ** Setup texture coord buffer
	glCreateBuffers(1, &textureCoordBuffer);
	glNamedBufferStorage(textureCoordBuffer, numPoints * sizeof(glm::vec2), textureCoordArray, 0);
	glVertexArrayVertexBuffer(vertexArrayObj, 2, textureCoordBuffer, 0, sizeof(glm::vec2));
	glVertexArrayAttribBinding(vertexArrayObj, 2, 2);
	glVertexArrayAttribFormat(vertexArrayObj, 2, 2, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vertexArrayObj, 2);

	// Setup index buffer
	glCreateBuffers(1, &indexBuffer);
	glNamedBufferStorage(indexBuffer, indexArraySize, indexArray, 0);
	glVertexArrayElementBuffer(vertexArrayObj, indexBuffer);

	// Clean-up
	free(positionArray);
	free(indexArray);


	surfaceShader = GLShaderFactory::CreateProgramObject(
		GLShaderFactory::CreateShader(GL_VERTEX_SHADER, string("shaders\\Tessellation\\terrain.vs.txt")),
		GLShaderFactory::CreateShader(GL_TESS_CONTROL_SHADER, string("shaders\\Tessellation\\terrain.tcs.txt")),
		GLShaderFactory::CreateShader(GL_TESS_EVALUATION_SHADER, string("shaders\\Tessellation\\terrain.tes.txt")),
		GLShaderFactory::CreateShader(GL_FRAGMENT_SHADER, string("shaders\\Tessellation\\terrain.fs.txt"))
	);


	// ** Load terrain textures to apply multi-texturing
	grassTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\Hodges_G_Grass3.jpg", TextureProperties(true));
	waterTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\photos_2017_11_10_fst_sea-ocean-water-texture.jpg", TextureProperties(true));
	rockTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\Hodges_G_MountainRock1.jpg", TextureProperties(true));
	snowTexture = fiLoadTexture("..\\shared_resources\\Textures\\Terrain\\LLoyd_Jordan_snow1.jpg", TextureProperties(true));

	TerrainTexture T;

	T.grass.texture = glGetTextureHandleARB(grassTexture);
	T.water.texture = glGetTextureHandleARB(waterTexture);
	T.rock.texture = glGetTextureHandleARB(rockTexture);
	T.snow.texture = glGetTextureHandleARB(snowTexture);
	T.proGen.texture = glGetTextureHandleARB(procTexture);

	T.grass.scale = glm::vec2(6.0f, 6.0f);
	T.water.scale = glm::vec2(8.0f, 8.0f);
	T.rock.scale = glm::vec2(6.0f, 6.0f);
	T.proGen.scale = glm::vec2(16.0f, 16.0f);
	T.snow.scale = glm::vec2(6.0f, 6.0f);

	// ** Setup texture scale buffer
	glCreateBuffers(1, &textureModelBuffer);
	glNamedBufferStorage(textureModelBuffer, sizeof(TerrainTexture), &T, 0);

	glMakeTextureHandleResidentARB(T.grass.texture);
	glMakeTextureHandleResidentARB(T.water.texture);
	glMakeTextureHandleResidentARB(T.rock.texture);
	glMakeTextureHandleResidentARB(T.snow.texture);
	glMakeTextureHandleResidentARB(T.proGen.texture);
	passThrough = false;

	//
	// Setup on-terrain features
	//

	treeField = new TreeField(500, domainCoord, domainExtent, fractalLambda);
}


void PatchTerrain::render(ArcballCamera* camera, GLuint procTexture) {

	// ** Bind uniform buffer with texture handles and scale factors
	/*
	if (passThrough) {
		TerrainTexture T;

		T.grass.texture = glGetTextureHandleARB(procTexture);
		T.rock.texture = glGetTextureHandleARB(procTexture);
		T.snow.texture = glGetTextureHandleARB(procTexture);

		T.proGen.texture = glGetTextureHandleARB(procTexture);
		T.grass.scale = glm::vec2(6.0f, 6.0f);
		T.rock.scale = glm::vec2(6.0f, 6.0f);
		T.snow.scale = glm::vec2(6.0f, 6.0f);

		// ** Setup texture scale buffer
		glCreateBuffers(1, &textureModelBuffer);
		glNamedBufferStorage(textureModelBuffer, sizeof(TerrainTexture), &T, 0);

		glMakeTextureHandleResidentARB(T.grass.texture);
		glMakeTextureHandleResidentARB(T.rock.texture);
		glMakeTextureHandleResidentARB(T.snow.texture);
		glMakeTextureHandleResidentARB(T.proGen.texture);


		
	}
	*/
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, textureModelBuffer);

	
	// Get the camera properties we need
	glm::mat4 viewTransform = camera->viewTransform();
	glm::mat4 projectionTransform = camera->projectionTransform();
	float nearPlaneDist = camera->getViewFrustum().nearPlaneDistance();
	float farPlaneDist = camera->getViewFrustum().farPlaneDistance();

	glBindVertexArray(vertexArrayObj);

	// Bind surface shader and assign uniform values
	GLuint shader = surfaceShader->useProgramObject();// surfaceShader->useProgram();

	static GLint viewLocation = glGetUniformLocation(shader, "viewMatrix");
	static GLint projLocation = glGetUniformLocation(shader, "projMatrix");
	static GLint nearLocation = glGetUniformLocation(shader, "near");
	static GLint farLocation = glGetUniformLocation(shader, "far");

	glUniformMatrix4fv(viewLocation, 1, GL_FALSE, (const GLfloat*)&viewTransform);
	glUniformMatrix4fv(projLocation, 1, GL_FALSE, (const GLfloat*)&projectionTransform);
	glUniform1f(nearLocation, nearPlaneDist);
	glUniform1f(farLocation, farPlaneDist);

	// Draw terrain using triangle patches for tessellation
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPatchParameteri(GL_PATCH_VERTICES, 3);
	glDrawElements(GL_PATCHES, numIndices, GL_UNSIGNED_INT, (const GLvoid*)0);


	// Render on-surface features
	if (treeField) {

		treeField->render(camera);
	}

	//passThrough = true;
}


PatchTerrain::PatchTerrain(GLuint initTexture) {

	loadShader();
	//setupVAO();

	ProGenTexture = initTexture;
}

void PatchTerrain::loadShader() {
	

	tempShader = setupShaders(string("Shaders\\basic_texture.vs.txt"), string("")
		, string("Shaders\\basic_texture.fs.txt"));
}
